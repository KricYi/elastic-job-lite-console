### elastic-job-lite-console使用方式

1. elastic-job-lite-console是经过编译的，用git命令下载到本地

```sybase
git clone https://gitee.com/yuejuncheng/elastic-job-lite-console.git
```

2. 下载完成后，解压到指定目录。如果是在Windows中运行，执行bin目录下的start.bat文件；如果是在linux中运行，则执行bin目录下的start.sh文件

3. 运行成功后，在浏览器中输入访问地址http://127.0.0.1:8899，默认账号和密码都是root。如果要修改账号和密码，可修改config目录下的auth.properties文件

4. .进入UI管理界面后，配置注册中心，然后就可以看到相应的任务。

![1605092637881](https://gitee.com/yuejuncheng/blogImg/raw/master/images/20201217134502.png)

* 注册中心名称：一般为项目名称，如：elastic-job-demo
* 注册中心地址：zookeeper的连接地址
* 命名空间：一般为项目名称，如：elastic-job-demo
* 登录凭证：可以省略不填